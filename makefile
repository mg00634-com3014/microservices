# Microservices Project Make File
# author: umer mansoor

VIRTUALENV = $(shell which virtualenv)

com3014_setup:
	sudo apt-get update
	sudo apt-get install build-essential checkinstall
	sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
	sudo cp ~/Documents/microservices/Python-2.7.18.tgz /usr/src/
	sudo tar xzf /usr/src/Python-2.7.18.tgz
	sudo /usr/src/Python-2.7.18/configure --enable-optimizations
# install as python2.8 instead of python
	sudo make -C /usr/src/Python-2.7.18/ altinstall
	python2.7 ~/Documents/microservices/get-pip.py
	pip install virtualenv

clean: shutdown
	rm -fr ./microservices.egg-info
	rm -fr venv

venv:
	echo "$(shell which virtualenv)"
	$(VIRTUALENV) venv

install: clean venv
	. venv/bin/activate; python2.7 setup.py install
	. venv/bin/activate; python2.7 setup.py develop
	. venv/bin/activate; pip install requests
	. venv/bin/activate; pip install flask

launch: venv shutdown
	. venv/bin/activate; python2.7  services/movies.py &
	. venv/bin/activate; python2.7  services/showtimes.py &
	. venv/bin/activate; python2.7  services/bookings.py &
	. venv/bin/activate; python2.7  services/user.py &

shutdown:
	ps -ef | grep "services/movies.py" | grep -v grep | awk '{print $$2}' | xargs --no-run-if-empty
	ps -ef | grep "services/showtimes.py" | grep -v grep | awk '{print $$2}' | xargs --no-run-if-empty
	ps -ef | grep "services/bookings.py" | grep -v grep | awk '{print $$2}' | xargs --no-run-if-empty
	ps -ef | grep "services/user.py" | grep -v grep | awk '{print $$2}' | xargs --no-run-if-empty

